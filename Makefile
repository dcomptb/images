# =============================================================================
# DCompTB images build script
# ---------------------------
#
#  This makefile builds the currently supported dcomp base images from
#  upstream OS base images. Packer is used to bring up the base OS image
#  in a QEMU virtual machine and add the dcomp public key as well as ensuing
#  python is installed for ansible usage.
#
# =============================================================================

.PHONY: all
all: ubuntu

base:
	mkdir -p base

.PHONY: clean
clean: ubuntu-clean
	sudo rm -rf build

packer=`which packer`
me=`whoami`

### ===========================================================================
### Ubuntu --------------------------------------------------------------------
### ===========================================================================

.PHONY: ubuntu
ubuntu: ubuntu1804

### Ubuntu 18.04 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.PHONY: ubuntu1804
ubuntu1804: build/ubuntu/1804/ubuntu1804

UBUNTU_MIRROR=https://cloud-images.ubuntu.com
UBUNTU_1804=ubuntu-18.04-server-cloudimg-amd64.img
UBUNTU_URL=${UBUNTU_MIRROR}/releases/18.04/release/${UBUNTU_1804}

build/ubuntu/1804/ubuntu1804: ubuntu-1804.json cloud-init/ubuntu-1804-config.iso | build/ubuntu base/${UBUNTU_1804} 
	sudo rm -rf build/ubuntu/1804
	sudo -E ${packer} build ubuntu-1804.json
	sudo chown ${me}:${me} -R build

cloud-init/ubuntu-1804-config.iso: cloud-init/build-iso-ubuntu.sh cloud-init/ubuntu1804/user-data cloud-init/ubuntu1804/meta-data 
	cd cloud-init; ./build-iso-ubuntu.sh

build/ubuntu:
	sudo mkdir -p build/ubuntu

base/$(UBUNTU_1804): | base
	wget --directory-prefix base ${UBUNTU_URL}

.PHONY: ubuntu-clean
ubuntu-clean:
	rm -rf cloud-init/*ubuntu*.iso

### ===========================================================================
### Debian --------------------------------------------------------------------
### ===========================================================================

### ===========================================================================
### Fedora --------------------------------------------------------------------
### ===========================================================================

### ===========================================================================
### CentOS --------------------------------------------------------------------
### ===========================================================================

### ===========================================================================
### FreeBSD -------------------------------------------------------------------
### ===========================================================================

