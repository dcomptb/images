#!/bin/bash

set -e

opts="-volid cidata -joliet -rock"
input="user-data meta-data"

cd ubuntu1804
genisoimage -output ubuntu-1804-config.iso $opts $input
mv ubuntu-1804-config.iso ../
cd ..
