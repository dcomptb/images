#!/bin/sh

set -e
set -x

mkdir -p /home/dcomp-init/.ssh

curl -fsSLo /home/dcomp-init/.ssh/authorized_keys \
  https://gitlab.com/dcomptb/dcomptb/raw/master/keys/dcomp_rsa.pub?inline=false

chmod 700 /home/dcomp-init/.ssh/
chmod 600 /home/dcomp-init/.ssh/authorized_keys
chown -R dcomp-init:dcomp-init /home/dcomp-init
